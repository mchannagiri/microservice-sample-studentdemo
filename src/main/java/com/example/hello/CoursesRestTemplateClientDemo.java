package com.example.hello;

import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/api/student/courses")
public class CoursesRestTemplateClientDemo {

  Logger logger = Logger.getLogger(CoursesRestTemplateClientDemo.class.getName());


  @Autowired
  RestTemplate restTemplate;

   @Autowired
   private Tracer tracer;

  // @Autowired
  // Sampler sampler;

//  @NewSpan
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String getCourses() {
    // Span newSpan = tracer.createSpan(" Class:CoursesRestTemplateClientDemo");
    // newSpan.tag(newSpan.traceIdString(), "getCourses method");
    // tracer.close(newSpan);
    
    logger.info("In StudentDemo Service , course rest template class,getCourses method .....");

    ResponseEntity<String> restExchange = restTemplate
        .exchange("http://localhost:8765/api/courses/", HttpMethod.GET, null, String.class);

    return restExchange.getBody();
  }

//  @NewSpan
  @RequestMapping(value = "/{studentId}", method = RequestMethod.GET)
  public String getCourseInfo(@PathVariable("studentId") String studentId) {
    // Span newSpan = tracer.createSpan("Class: CoursesRestTemplateClientDemo");
    // newSpan.tag(newSpan.traceIdString(), "getCourseInfo");
    // tracer.close(newSpan);


    logger.info("In StudentDemo Service , course rest template class,getCourseInfo method .....");
    ResponseEntity<String> restExchange =
        restTemplate.exchange("http://localhost:8765/api/courses/{studentId}", HttpMethod.GET, null,
            String.class, studentId);

    return restExchange.getBody();
  }



}


