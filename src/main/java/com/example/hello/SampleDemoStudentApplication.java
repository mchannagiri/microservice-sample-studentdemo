package com.example.hello;

import java.util.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SampleDemoStudentApplication {
  static Logger logger = Logger.getLogger(SampleDemoStudentApplication.class.getName());

  @Bean
  public Sampler defaultSampler() {
    return new AlwaysSampler();
  }


  public static void main(String[] args) {
    SpringApplication.run(SampleDemoStudentApplication.class, args);
  }

  @LoadBalanced
  @Bean
  public RestTemplate getRestTemplate() {
    RestTemplate template = new RestTemplate();
    return template;
  }

  @Bean
  public InjectTraceFilter injectTraceFilter() {
    return new InjectTraceFilter();
  }
}


