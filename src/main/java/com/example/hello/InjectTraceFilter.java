package com.example.hello;

import java.io.IOException;
import java.util.Random;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.Span;
import org.springframework.cloud.sleuth.Tracer;
import org.springframework.cloud.sleuth.instrument.web.TraceFilter;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

@Component
@Order(-1)
public class InjectTraceFilter extends GenericFilterBean {

  private static final String TRACE_REQUEST_ATTR = TraceFilter.class.getName() + ".TRACE";
  private final Random random = new Random(System.currentTimeMillis());

  @Autowired
  private Tracer tracer;

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    final HttpServletRequest httpRequest = (HttpServletRequest) request;
    Span span = spanForId(httpRequest.getHeader("tenantId"));
    httpRequest.setAttribute(TRACE_REQUEST_ATTR, span);
    chain.doFilter(httpRequest, response);
    tracer.close(span);

  }

  private Span spanForId(final String traceId) {
    return Span.builder().traceId(Span.hexToId(traceId)).spanId(random.nextLong()).exportable(true)
        .build();
  }

}
